#include "Bullet.h"
#include"AnimatingObject.h"
#include "AssetManager.h"

Bullet::Bullet(sf::Texture & bulletTexture,
	sf::Vector2u newScreenSize,
	sf::Vector2f startingPosition,
	sf::Vector2f newVelocity)
	:
	AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/bullet.png"), 20, 20, 5.0f),//create 3d bullet
	screenSize(newScreenSize),
	velocity(newVelocity)
{
	sprite.setPosition(startingPosition);
	sprite.setRotation(90.0f);
	alive = true;
	bulletSoundBuffer.loadFromFile("Assets/Audio/gunshot.ogg");
	bulletFireSound.setBuffer(bulletSoundBuffer);
	bulletFireSound.setVolume(25);
	//const sf::Texture* tempTexture = sprite.getTexture();
}

void Bullet::Update(sf::Time frameTime)
{
	
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	//check if bullet is off screen
	if (newPosition.x + sprite.getTexture()->getSize().x < 0 || newPosition.x > screenSize.x)
	{
		alive = false;
		//delete bullet when off screen

	}
	if (newPosition.y + sprite.getTexture()->getSize().y < 0 || newPosition.y > screenSize.y)
	{
		alive = false;
		//delete bullet when off screen

	}

	//set new position
	sprite.setPosition(newPosition);
}


bool Bullet::GetAlive()
{
	return alive;
}

sf::FloatRect Bullet::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Bullet::SetAlive(bool newAlive)
{
	alive = newAlive;
}
