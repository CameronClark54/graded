#pragma once
#include "AnimatingObject.h"
#include <vector>
#include <SFML/Audio.hpp>
class Bullet :
	public AnimatingObject
{
public:
	//constructor
	Bullet(sf::Texture& bulletTexture, sf::Vector2u newScreenSize, sf::Vector2f startingPosition, sf::Vector2f newVelocity);

	//functions
	void Update(sf::Time frameTime);
	

	// Getters
	bool GetAlive();
	sf::FloatRect GetHitBox();

	//Setters
	void SetAlive(bool newAlive);

private:
	// Variables
	
	sf::Vector2f velocity;
	sf::Vector2u screenSize;
	bool alive;
	sf::SoundBuffer bulletSoundBuffer;
	sf::Sound bulletFireSound;
};

