#include "Player.h"
#include "AssetManager.h"
#include "Bullet.h"
#include <cmath>


Player::Player( sf::Vector2u screenSize, std::vector<Bullet>& newBullets, sf::Texture & newBulletTexture, sf::SoundBuffer & firingSoundBuffer)
	:AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 96, 128, 5.0f)//Change
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, bullets(newBullets)
	, bulletTexture(newBulletTexture)
	, bulletCooldownRemaining(sf::seconds(0.0f))
	, bulletCooldownMax(sf::seconds(0.5f))
	, bulletFireSound(firingSoundBuffer)


{

	//animates player movement
	AddClip("walkRight", 0, 0);
	AddClip("walkLeft", 1, 1);
	PlayClip("walkRight", true);

	// position the player at the centre of the screen
	sf::Vector2f newPosition;
	newPosition.x = ((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f;
	newPosition.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(newPosition);

	deathSoundBuffer.loadFromFile("Assets/Audio/game_end.ogg");
	Death.setBuffer(deathSoundBuffer);
	Death.setVolume(25);

	isAlive = true;
	

	const sf::Texture* tempTexture = sprite.getTexture();
}


void Player::Input(sf::Vector2u screenSize)
{
	// Player keybind input
	velocity.x = 0.0f;
	velocity.y = 0.0f;
	bool hasInput = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;

		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
		//plays walking left animation
		if (hasInput == false)
		{
			PlayClip("walkLeft",true);
		}
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
		//plays walking right animation
		if (hasInput == false)
		{
			PlayClip("walkRight",true);
		}
		hasInput = true;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && bulletCooldownRemaining <= sf::seconds(0.0f))
	{
		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture.getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) 
		{
			bullets.push_back(Bullet(bulletTexture, screenSize, bulletPosition, sf::Vector2f(0, -1000)));
			// Play firing sound
			bulletFireSound.play();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			bullets.push_back(Bullet(bulletTexture, screenSize, bulletPosition, sf::Vector2f(0, 1000)));
			// Play firing sound
			bulletFireSound.play();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			bullets.push_back(Bullet(bulletTexture, screenSize, bulletPosition, sf::Vector2f(-1000, 0)));
			// Play firing sound
			bulletFireSound.play();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			bullets.push_back(Bullet(bulletTexture, screenSize, bulletPosition, sf::Vector2f(1000, 0)));
			// Play firing sound
			bulletFireSound.play();
		}

		

		// reset bullet cooldown
		bulletCooldownRemaining = bulletCooldownMax;
	}

	if (hasInput == false)
	{
		Stop();
	}
}

void Player::Update(sf::Time frameTime, sf::Vector2u screenSize)
{
	//sets previous position
	previousPosition = sprite.getPosition();

	//calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	//check if the left of the sprite is off the screen to the left
	if (newPosition.x < 0)
	{
		newPosition.x = 0;
	}

	//Check if the right side of the sprite is off the right side of the screen
	if (newPosition.x + sprite.getTexture()->getSize().x > screenSize.x)
	{
		newPosition.x = screenSize.x - sprite.getTexture()->getSize().x;
	}

	//check if the top of the sprite is off the screen to the top
	if (newPosition.y < 0)
	{
		newPosition.y = 0;
	}

	//check if the bottom of the sprite is off the screen to the bottom
	if (newPosition.y + sprite.getTexture()->getSize().y > screenSize.y)
	{
		newPosition.y = screenSize.y - sprite.getTexture()->getSize().y;
	}

	//move player to new position
	sprite.setPosition(newPosition);

	currentPosition = newPosition;
	

	//allows for the player animation to update
	AnimatingObject::Update(frameTime);

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;
}

bool Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	//if player collides with enemy
	if (GetHitbox().intersects(otherHitbox))
	{
		isAlive = false;
		Death.play();
		
	}
	return isAlive;
}



bool Player::GetAlive()
{
	return isAlive;
}

sf::Vector2f Player::position()
{
	return currentPosition;
}
