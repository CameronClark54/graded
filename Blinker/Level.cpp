#include "Level.h"
#include "Game.h"
#include "AssetManager.h"
#include <stdlib.h>
#include <iostream>
#include <fstream>



Level::Level(Game* newGamePointer)
	:playerInstance(newGamePointer->GetWindow().getSize(),Bullets, AssetManager::RequestTexture("Assets/Graphics/bullet.png"),AssetManager::RequestSoundBuffer("Assets/Audio/gunshot.ogg"))
	, gamePointer(newGamePointer)
	, Fighters()
	, Bullets()
	, EnemiesGap(0)
	, score(0)
	, gameFont(AssetManager::RequestFont("Assets/Fonts/mainFont.ttf"))
{
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(50);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(400, 20);
	
	gameOverText.setFont(gameFont);
	gameOverText.setString("GAME OVER");
	gameOverText.setCharacterSize(200);
	gameOverText.setFillColor(sf::Color::White);
	gameOverText.setPosition(400, 400);

	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.setVolume(25);
	gameMusic.play();

	
}

void Level::Input(sf::Vector2u screenSize)
{
	playerInstance.Input(screenSize);
}

void Level::Update(sf::Time frameTime, sf::Vector2u screenSize)
{
	if (playerInstance.GetAlive() == true)
	{
		
		playerInstance.Update(frameTime,screenSize);
		for (int i = 0; i < Fighters.size(); ++i)
		{
			playerInstance.HandleSolidCollision(Fighters[i]->GetHitbox());
		}

		//updates and displays score
		scoreText.setString("Score: " + std::to_string((int)score));

		EnemiesGap = EnemiesGap + frameTime.asSeconds();

		if (EnemiesGap > 0.75)
		{
			AddEnemies();
		}

		//update Enemies
		for (int i = 0; i < Fighters.size(); ++i)
		{
			Fighters[i]->Update(frameTime, screenSize);

			if (!Fighters[i]->GetAlive())
			{
				Fighters.erase(Fighters.begin() + i);
			}
		}

		//Update bullets
		for (int i = Bullets.size() - 1; i >= 0; --i)
		{
			
			Bullets[i].Update(frameTime);

			//if bullet is off screen then delete it
			if (!Bullets[i].GetAlive())
			{
				// Remove the item from the vector
				Bullets.erase(Bullets.begin() + i);
			}
		}

		//check for collisions between player bullets and enemies
		for (int i = 0; i < Bullets.size(); ++i)
		{
			for (int j = 0; j < Fighters.size(); ++j)
			{
				sf::FloatRect bulletBounds = Bullets[i].GetHitBox();
				sf::FloatRect enemyBounds = Fighters[j]->GetHitBox();

				if (bulletBounds.intersects(enemyBounds))
				{
					//they intersect so delete them
					Bullets[i].SetAlive(false);
					Fighters[j]->SetAlive(false);
					score=score+10;


				}
			}

		}

	}
	else
	{
		gameMusic.stop();

	}


}

void Level::DrawTo(sf::RenderTarget & target)
{
	

	if (playerInstance.GetAlive() == true) 
	{
		playerInstance.DrawTo(target);

		for (int i = 0; i < Fighters.size(); ++i)
		{
			Fighters[i]->DrawTo(target);
		}

		for (int j = 0; j < Bullets.size(); ++j)
		{
			Bullets[j].DrawTo(target);
		}
	}

	gamePointer->GetWindow().draw(scoreText);

	if (playerInstance.GetAlive() == false) 
	{
		gamePointer->GetWindow().draw(gameOverText);
	}
}

void Level::AddEnemies()
{
	//creates a new Enemies
	Enemy* newEnemies = nullptr;

	//chooses the type of Enemies to create
	int choiceMin = 0;
	int choiceMax = 100;
	int choice = rand() % (choiceMax - choiceMin) + choiceMin;
	int chanceMoving = 50;
	if (choice < 50)
	{
 		newEnemies = new OtherEnemy(&playerInstance);
		Fighters.push_back(newEnemies);
	}
	else
	{
		newEnemies = new Enemy(&playerInstance);
		Fighters.push_back(newEnemies);
	}
	EnemiesGap = 0;
}



