#include "Enemy.h"
#include "AssetManager.h"

Enemy::Enemy(Player* newPlayerPointer)
	:SpriteObject(AssetManager::RequestTexture("Assets/Graphics/fighter1.png"))
	, velocity(150)
	,playerPointer(newPlayerPointer)
	
{
	sprite.setPosition(0, 550);
	alive = true;
}

Enemy::Enemy(sf::Texture & newTexture, sf::Vector2f newPosition, Player* newPlayerPointer)
	: SpriteObject(newTexture)
	, velocity(150)
	, playerPointer(newPlayerPointer)
{
	sprite.setPosition(newPosition);
	alive = true;
}

void Enemy::Update(sf::Time frameTime, sf::Vector2u screenSize)
{
	
	sf::Vector2f newPos = sprite.getPosition();
	float playerX = playerPointer->position().x;
	float playerY = playerPointer->position().y;
	if (playerX<newPos.x)
	{
		newPos.x -= velocity * frameTime.asSeconds();
	}
	if (playerY < newPos.x)
	{
		newPos.y -= velocity * frameTime.asSeconds();
	}
	if (playerX > newPos.x)
	{
		newPos.x += velocity * frameTime.asSeconds();
	}
	if (playerY > newPos.y)
	{
		newPos.y += velocity * frameTime.asSeconds();
	}
	
	sprite.setPosition(newPos);
}

bool Enemy::GetAlive()
{
	return alive;
}

sf::FloatRect Enemy::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Enemy::SetAlive(bool newAlive)
{
	alive = newAlive;
}
