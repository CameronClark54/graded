#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include"Enemy.h"
#include"OtherEnemy.h"
#include <vector>
#include <SFML/Audio.hpp>
#include <string>

class Game;

class Level
{
public:
	//constructor
	Level(Game* newGamePointer);
	void Input(sf::Vector2u screenSize);
	void Update(sf::Time frameTime, sf::Vector2u screenSize);
	void DrawTo(sf::RenderTarget& target);
	void AddEnemies();

private:
	Player playerInstance;
	std::vector<Enemy*> Fighters;
	std::vector<Bullet>Bullets;
	float EnemiesGap;
	sf::Text scoreText;
	sf::Text gameOverText;
	sf::Font gameFont;
	sf::Music gameMusic;
	
	float score;
	Game* gamePointer;
};

