#pragma once
#include "SpriteObject.h"
#include <stdlib.h>
#include "Player.h"
class Enemy :
	public SpriteObject
{
public:
	// Constructors
	Enemy(Player* newPlayerPointer);
	Enemy(sf::Texture& newTexture, sf::Vector2f newPosition, Player* newPlayerPointer);
	virtual void Update(sf::Time frameTime, sf::Vector2u screenSize);
	
	// Getters
	bool GetAlive();
	sf::FloatRect GetHitBox();

	//Setters
	void SetAlive(bool newAlive);


private:
	int velocity;
	
	bool alive;
	Player* playerPointer;
	
};
