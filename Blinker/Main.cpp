#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>
#include "Game.h"
#include <stdlib.h>

int main()
{
	// declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Blinker", sf::Style::Titlebar | sf::Style::Close);

    
    //
    //GAME SETUP
    //
    
	Game gameInstance;
	//this will not end until the game is over
	gameInstance.RunGameLoop();
	//if we get here, the loop exited, so end the program by returning
	//return 0;




}