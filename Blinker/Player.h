#pragma once
#include "AnimatingObject.h"
#include "Bullet.h"
#include <SFML/Audio.hpp>
class Player :public AnimatingObject
{
public:
	// Constructors / Destructors
	Player( sf::Vector2u screenSize,  std::vector<Bullet>& newBullets, sf::Texture&newBulletTexture, sf::SoundBuffer& firingSoundBuffer);

	// Functions
	void Input(sf::Vector2u screenSize);
	void Update(sf::Time frameTime, sf::Vector2u screenSize);
	bool HandleSolidCollision(sf::FloatRect otherHitbox);


	//getter
	bool GetAlive();
	sf::Vector2f position();

private:
	// Data
	sf::Vector2f velocity;
	float speed;
	sf::Vector2f previousPosition;
	sf::Vector2f currentPosition;

	std::vector<Bullet>& bullets;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	bool isAlive;
	sf::SoundBuffer deathSoundBuffer;
	sf::Sound Death;
};
