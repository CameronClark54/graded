#include "AnimatingObject.h"

AnimatingObject::AnimatingObject(sf::Texture & newTexture, int newFrameWidth, int newFrameHeight, float newFPS) :
	SpriteObject(newTexture)
	, frameWidth(newFrameWidth)
	, frameHeight(newFrameHeight)
	, framesPerSecond(newFPS)
	, currentFrame(0)
	, timeInFrame(sf::seconds(0.0f))
	, clips()
	, currentClip("")
	, playing(false)
	, looping(true)
{
	const sf::Texture* tempTexture = sprite.getTexture();
}

void AnimatingObject::AddClip(std::string name, int startFrame, int endFrame)
{
	//create new animation
	Clip& newClip = clips[name];

	//setup settings for this animation
	newClip.startFrame = startFrame;
	newClip.endFrame = endFrame;
}

void AnimatingObject::PlayClip(std::string name, bool shouldLoop)
{
	//checks if current clip is already playing
	if (currentClip == name && playing == true)
	{
		//bail out
		return;
	}

	//find the clipss information in the map
	auto pairFound = clips.find(name);

	//if the clip exists
	if (pairFound != clips.end())
	{
		//set up animation based on clip
		currentClip = name;
		currentFrame = pairFound->second.startFrame;
		timeInFrame = sf::seconds(0.0f);
		playing = true;
		looping = shouldLoop;
		//update sprite rect
		UpdateSpriteTextureRect();
	}
	if (pairFound != clips.end() && currentClip == name)
	{
		Resume();
	}
}

void AnimatingObject::Pause()
{
	playing = false;
}

void AnimatingObject::Stop()
{
	playing = false;

	//reset to first frame of animation
	Clip& thisClip = clips[currentClip];
	currentFrame = thisClip.startFrame;
	UpdateSpriteTextureRect();
}

void AnimatingObject::Resume()
{
	if (!currentClip.empty())
	{
		playing = true;
	}
}

void AnimatingObject::Update(sf::Time frameTime)
{
	if (!playing)
	{
		return;
	}

	//if its time for new frame
	timeInFrame += frameTime;
	sf::Time timePerFrame = sf::seconds(1.0f / framesPerSecond);
	if (timeInFrame >= timePerFrame)
	{
		//update current frame
		++currentFrame;
		timeInFrame = sf::seconds(0);

		// If we get to end of clip
		Clip& thisClip = clips[currentClip];
		if (currentFrame > thisClip.endFrame)
		{
			// we loop
			if (looping == true)
			{
				//go to the start of the clip
				currentFrame = thisClip.startFrame;
			}
			else // we don't loop
			{
				//stay on last frame
				currentFrame = thisClip.endFrame;
				playing = false;
			}
		}

		//update sprite to use the correct frame of texture
		UpdateSpriteTextureRect();
	}
}

void AnimatingObject::UpdateSpriteTextureRect()
{

	//update sprite to use correct frame of the texture
	const sf::Texture* texture = sprite.getTexture();
	sf::Vector2u size = texture->getSize();
	unsigned int width = size.x;

	int numFramesX = width / frameWidth; //sprite.getTexture()->getSize().x / frameWidth;
	
	int xFrameIndex = currentFrame % numFramesX;
	int yFrameIndex = currentFrame / numFramesX;
	sf::IntRect textureRect;
	textureRect.left = xFrameIndex * frameWidth;
	textureRect.top = yFrameIndex * frameHeight;
	textureRect.width = frameWidth;
	textureRect.height = frameHeight;
	sprite.setTextureRect(textureRect);
}
